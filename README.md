# README #

This Document is detailed to setup the Global File Uploader Component in the Salesforce System.

### What is this repository for? ###

This repository contains all the metadata that has to deployed in the destination org inorder to access the Global file uploader component.

### How do I get set up? ###

Steps to setup the Global File Uploader Component.
1) Deploy the Component Repository in the org.
2) From Setup follow the below path to setup the Object specific details on which the object record the component will be used.
	Setup -> Type Custom Metadata in quick find box and click on select.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact